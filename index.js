/** interface Array {
 	    Array {
 		    name: String,
 			  type: String
 	    }
    }
 **/

/**
 *
 * @param events
 * @returns {boolean}
 */

function testService(events) {
	const checkedUsers = [];
	const eLength = events.length;

	if (eLength > 0) {
		for (let i = 0; i < eLength; i++) {
			if (events[i][1] !== 'in' || events[i][1] === 'out') {
				for (let j = 0; j < checkedUsers.length; j++) {
					if ((checkedUsers[j][0] === events[i][0])
						&& (checkedUsers[j][1] !== events[i][1])) {
						// removing correct shown events
						let idx = checkedUsers.indexOf(checkedUsers[j]);
						checkedUsers.splice(idx, 1);
					} else {
						return false;
					}
				}
			} else {
				if (checkedUsers.length === 0) {
					checkedUsers.push(events[i]);
				} else {
					for (let j = 0; j < checkedUsers.length; j++) {
						if ((checkedUsers[j][0] === events[i][0])
							&& (checkedUsers[j][1] === events[i][1])) {
							return false;
						}
					}
					//adding events to check their 'check-out's
					checkedUsers.push(events[i]);
				}
			}
		}
		return checkedUsers.length === 0;
	} else {
		// checking for empty data (array)
		return true;
	}

	//if the length of checkedUsers > 0 in the end ==> always false
}

module.exports = testService